

# Morphology

## Problema resuelto 1

![](.gitbook/assets/p1%20%281%29.svg )

The mechanism of the figure consists of a parallelogram transmission moving the joint of a robot.

The distance between the axis of the motor-spindle and the support of the joint remains constant at 250 mm, being the arm that appears to the left of the joint extensible to maintain that distance constant.

The parallelogram is moved by an AC motor with resolver, whose maximum torque is 0.3 N·m.

This motor-spindle assembly offers a reduction ratio of 2000 m-¹ which allows the transformation of the motor's rotation into the translation movement of the spindle, with an efficiency of 80%.

The equations of the resolver are:

 *  $$V1 = V\cdot\sin(\omega t)\cdot\sin(\theta_m)$$,
 *  $V2 = V\cdot\sin(\omega t)\cdot\cos(\theta _m)$,




